---
layout: page
title: About
permalink: /about/
---

Veronica is a designer from Melbourne with a background in communication and interaction design. I'm interested in the intersections of design, emergent technology, art, media, popular culture, cities, and communities.
