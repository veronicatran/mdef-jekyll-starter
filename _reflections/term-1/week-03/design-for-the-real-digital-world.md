---
number: 03
title: Design for the Real Digital World
period: 15-21 October 2018
date: 2018-10-21 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/digi-world-working.jpg)

_With Ingi Freyr and Francesco Zonca_

# Reflection

This week we collected trash from the streets of Poblenou, and worked in teams to create interventions to our workspace. The goal was to improve our classroom through repurposing found material, to optimize the space, allowing for comfort and joy for our day to day activities. Our space was to be adapted to our needs, keeping us, as designers inspired and curious; the objects taken from Poblenou/Sant Antoni connects us to our lab, both in and out of IAAC.

The tools we used throughout the process, as we hacked and redesigned away, included CNC milling, laser cutting, 3d printing, alongside manual labouring of sawing, drilling, sanding, etc. My previous experiences in fabrication have been very minor, so I learnt much of the mechanics within a product design cycle. The exposure to both mechanical and manual means of production helped develop my understanding of how things are built, and has an immediate impact on how I approach designing products in the future.

Throughout the week we were familiarised with IAAC workshop, staff, material, and machines. I enjoyed the design process alongside the pacing as it forced us all to act in a fast and focused manner, and learned of the variables that need to be considered - material, stability, space available, structure. In contrast to the strong theoretical components of last week, I learnt this week primarily by doing and learning from my peers.

I felt challenges within the timeline, efficiency, and unfamiliarity. The combination of staying explorative alongside a short deadline mitigated our ability to collectively make decisions fast, but it was a nevertheless fun and we were able to work towards a unified vision. I think it’ll be an ongoing journey for us to figure out how to best utilize our existing skills in a team, whilst allowing each other to learn without stepping on anyone’s toes.

This week enabled us to exchange skillsets and put us in a situation where it was essential to communicate and trust each other to move forward, for me I did feel like a sponge to new information – I’m familiar with the pacing and iterative nature of design, but less so in creating CAD files and working with materials. It's something I want to rectify and aim to work towards developing throughout MDEF– I hope to be able to develop my 3D modelling skills in the future to visualize my ideas better.

It was great to be able to explore first-hand the possibilities of repurposing objects from the streets and see the final space transformed in just a week, and makes me excited for the possibilities of building + hacking products further in the future, and I think the poetry of mixing old and new as we work together to develop ideas for the future is very fitting.


# Process
## Phase 1

![]({{site.baseurl}}/digi-world-brainstorm.png)
### Brainstorm and Analysis
Our group started off with a brainstorm of all potential keywords, ideas, pain points, products, and more, to begin the dialogue between ourselves. We then concurrently began to discuss and analyse further to critique the space to enable us to move towards the right direction.

![]({{site.baseurl}}/digi-world-floorplan.gif)
### Floor Plan
We then created a floor plan as a group to enable us to think holistically about what products would sit realistically in the space. The final floor plan emphasized the flow of movement, alongside a combination of fixed and modular spaces in the classroom. These spaces consisted of table arrangement, modes of sitting/standing, working with the pillars, and presentation.

![]({{site.baseurl}}/digi-world-areas.png)
### Ideation
After having an idea for a proposed floor plan, we categorized our space into key themes that our potential products would be housed under – Workspace, Presentation Space, Chillspace, Fab Lab, and Storage. We worked primarily with reference materials, sketches, and 3D models to visualize and describe our ideas to each other.

![]({{site.baseurl}}/digi-world-floorplan-ideation-sketch.jpg)
### Concepts
We <a href="{{site.baseurl}}/digi-world-preso.pdf" download>presented</a> our concepts and were allocated the development of ‘Chair with Table’ and ‘Pillar integration’ after the group vote to distribute tasks.

## Phase 2

![]({{site.baseurl}}/digi-world-team-4.gif)
### Concept Development
Before moving ahead with our given products, we continued to brainstorm in an attempt to deconstruct and develop our product further. The potential directions were variations and individual interpretations of how we could see this product evolving, and tried to identify the exact goal of our modular station – was it to create division to give us individual workspaces? Aid in storage? Function on multiple scales? We did so by having an ongoing dialogue, mixed with sketches, models, and reference material, however reverted back to the initial concept. Keywords in our directions included japanese joinery, interlocking, modular, puzzles, and infil.

![]({{site.baseurl}}/digi-world-large-cnc.png)
![]({{site.baseurl}}/digi-world-single-cnc.png)
### Final Concept
By going through the development I feel that we had a stronger rationale when we reverted backwards to the original, despite the time that it costed. The idea was to enable the station to work at varying degrees of scale, as well as vertical and horizontal.

On its own when sitting on a table, this piece would act as storage/organization in a multitude of ways through the addition of slits/holes, that allow space for stationery/drinks/plugs/laptops, additional 3D printed storage, and space for fabric to flow through, and create a hammock for our objects. When placed vertically, it acts as a side desk for your laptop. It can be set together with others to create larger tables, or when all 9 are compiled, into one large circular table. Our key 'must have' lied in the modularity of the system that informed the design.

During this phase we also aimed to finetune what to do with the joints; and sketched out multiple versions of what would be most optimal in terms of both aesthetics and stability.

![]({{site.baseurl}}/digi-world-class.jpg)
### Prototyping
We laser cutted some small scale models to test our ideas before moving forward with the final piece. By having tangible pieces of material we were able to test each potential permutation of the design and work towards a more refined shape. We tested three shapes and two sets of joints.


<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/297581294?autoplay=1&loop=1&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
![]({{site.baseurl}}/digi-world-cnc.jpg)
### Production
We then moved towards production which combined CNC milling and 3D printing. At this point our group split into smaller groups and ended up working in a parallel manner; working on the furniture and pillar integration in the same time. One group became two ( table + pillars) which became three (table + pillar 1 + pillar 2), and we came together towards the end to finalize our designs.

The station was more design focused and the pillar was more intervention/material focused, as it was informed by the material from the beginning, whereas the modular station underwent a more ‘typical’ design process. In the end I think our processes were a mix of conventional and unconventional, with a combination of linear, circular, and improvised actions had to be completed for us to finish this process. Whether it be sketching > modelling > cutting, to sourcing materials last minute, to assisting eachother whenever possible.

![]({{site.baseurl}}/digi-world-table-graded.jpg)
![]({{site.baseurl}}/digi-world-table-sequence.gif)
###### Modular Station
Utility first and foremost, it acts as an aid in several contexts within the classroom– laptop holder, table, bench, seat, presentation aid, etc. The design aims to be adaptable and open to further evolution. A key point in the discussions was to allow for further customization by the individual owner; they can design and produce their own elements to be added onto the station and intervene according to the nature of the individual, whether it be 3D printed, with material, or something else.

![]({{site.baseurl}}/digi-world-pillars.jpg)
###### Pillars

The first pillar is compiled of three levels of shelving, re-built from one shelf – a level for power boards, that move up towards a mobile phone charging station with holes for USBs, and then a static shelf for miscellaneous items. The second is made up of a  variety of surfaces; chalkboard paint, mirror, and cork, for the class to contribute to as they wish.

# Overall

Between transforming objects from the streets of Barcelona, using IAAC facilities, navigating our individual sensibilities as a team, experimenting with our found materials, this week was highly intense and highly rewarding to me.

I've been able to start identifying my own strengths and weaknesses which naturally flowed onto the next week of exploring our own profiles. I'd like to familiarise myself with digital fabrication processes and combine that knowledge with my existing ways of thinking, to find opportunities with a new set of tools.

I see the streets with a new light and have greater appreciation for the details that come with this process. I think as a whole I can meet the challenges of this week with a lot of optimism on the trajectory of my personal development, as well as the development of MDEF and the MDEF cohort.



# Links and Resources
Photo References: Ollie J, Barbara D  

[Distributed Design](http://distributeddesign.eu/ "Distributed Design")  
[Casa Yuk](http://helloyok.com/ "Yok")  
[Ikea Hackers](https://www.ikeahackers.net/ "Ikea Hackers")  
[Federica Sala](http://www.salafederica.it/ "Federica Sala")  
[Daniel Emma](https://www.daniel-emma.com/  
)  
[Nasya ​Kopteva ​and Sasha​ Braulov  ](http://52factory.ru/ "Google's Homepage")  
[Computational Interlocking](https://www.rethinktokyo.com/2018/08/08/tsugite-japanese-joinery-wood  
https://www.cs.tau.ac.il/~dcor/articles/2015/computational.interlocking.pdf "Google's Homepage")  
