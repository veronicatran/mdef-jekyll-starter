---
number: 05
title: Navigating the Uncertainity
period: 29 October - 4 November 2018
date: 2018-11-04 12:00:00
term: 1
published: true
---

This week was made up of three lectures from Jose Luis de Vicente, Pau Alsina, and Mariana/Tomas. We were encouraged to ponder and debate the world crisis through the lens of experts, to begin to narrow down our vision and iterate upon our goals for this masters. A lot of information was given alongside a multitude of questions regarding complex, abstract issues.

How do I navigate MDEF, my career, and my intentions without getting lost? We were advised to ground ourselves by looking first inward, at our strengths and weaknesses, before outwards, and see who and what we can decide to affect. To balance the meandering with not getting lost when it comes to myself, my values, my history and my core.

There was an interesting note from Tomas regarding the concepts of ’Fluidity and recognition’, as reading last week’s documentation made me step back and think that perhaps I should be less prescriptive of certain areas I want to work in, and how to go about being less label-focused, and be led more so by the underlying values of why I wish to do something. From that core, maybe it will be easier to adapt to a fluid pathway and not force myself in any boxes.

My interests lie within emerging technologies, culture, the web, our personal spaces, our connection with the environment. Things that have been introduced to me thus far and within this week are an added layer of depth. The added language and knowledge to be able to engage with such abstract world issues. The weaving between different topics; climate change and time, material and politics, interfaces and civilisation, and all permutations thereof.

# Recount

![]({{site.baseurl}}/nu-earth.gif)


## [Jose Luis de Vicente](https://iaac.net/people/jose-luis-de-vicente/)  

Jose is a curator and researcher who came in to talk to us about today’s current climate crisis and how we can observe/measure it on the scale of time and reality; how can we function in the now? How can we engage further with the radically changing systems of the planet? What can we as designers do in this current state of global temperature rise?

He detailed the two most frequent visions for the future – Sustainable Utopias VS Apocalyptic Dystopias. A critique of the utopia lied within the potential for scalability, and the inherent western societal norms that would be embedded in such a world. The Anthropocene was also another point of conversation, and how humans have impacted the interface of the Earth through industrialization and pollution.

Future POVs

* Reductionist  
* Capitalist
* Accelerationist
* Extinctionist
* Mutualist
* +
* Optimist
* Opportunistic

The three tiers of design that we float through are sustainable product design, to thinking at a systems level, and now we aim to design interventions, to ‘not solve, but dissolve’ problems. How can we focus less so on a radical change, and more on a ‘significant adjustment’?

Discussions around the pessimistic nature of the talk followed during the debrief with Mariana and Tomas. Why do we have a default pessimistic moral code on the nature of climate change, and how do we design for a new set of parameters with a more optimistic outlook? How do we design with new systems in mind instead of ‘stopping and reversing’ the changes, so that we may design for a new set of parameters and design to live in a new situation. We were encouraged to work within our values and engage with the potential for new ideas and see the function of the whole of the system, citing the Cambrian Explosion as an example of opportunity for creativity in nature.

[Després de la fi del món](http://www.cccb.org/ca/exposicions/fitxa/despres-de-la-fi-del-mon/224747)  
[Ministry of the Future](http://www.cccb.org/rcs_gene/timothy_morton_en.pdf)  
[Anthropocene](https://en.wikipedia.org/wiki/Anthropocene)  
[Ed Hawkins](https://twitter.com/ed_hawkins?lang=en)  
[Superflux](http://superflux.in/)  
[Hyper Object](https://www.upress.umn.edu/book-division/books/hyperobjects)  
[Aquapioneers](http://aquapioneers.io/)  
[Fab Academy Archive](http://archive.fabacademy.org/)  
[Precious Plastics](https://preciousplastic.com/)  
[Project ARA](https://en.wikipedia.org/wiki/Project_Ara)  
[How to Transform Apocalypse Fatigue into Action on Global Warming](https://www.ted.com/talks/per_espen_stoknes_how_to_transform_apocalypse_fatigue_into_action_on_global_warming/up-next?language=en)  
[Tom Hegen](https://www.itsnicethat.com/articles/tom-hegen-salt-series-photography-080518)  
[Reducing Emissions Alone Won't Stop Climate Change](http://theconversation.com/reducing-emissions-alone-wont-stop-climate-change-new-research-45493)  
[The Great Acceleration](https://anthropocenejournal.wordpress.com/2011/12/22/the-great-acceleration/#jp-carousel-4)

![]({{site.baseurl}}/nu-brain.gif)

## [Pau Alsina](https://twitter.com/paualsina?lang=en)

On Tuesday Pau opened the discussion to revolve around the paper ‘Unfolding the political capacities of design’. It asks, ‘What are the political capacities of design?’, and seeks to unfold the narratives of our material environment.

One such example was about the importance of space and placement considerations in context; in a classroom – what if the teacher was placed in the middle? What implications are there on the power of the teacher by placing him at the front, what does that say about the distribution of power when all eyes are on a singular human; what changes can be shifted if we amend this placement?

![]({{site.baseurl}}/nu-nendo.jpg)
[Nendo Manga Chairs](http://www.nendo.jp/en/works/50-manga-chairs/50-manga-chairs-3/)

He discussed the chair as a material insertion of disciplinary institution - it is a microtechnology. We as citizens are constructed through education and these dictate the behaviour and social control of our society. See the chair as a tool for behaviour and mechanisms of civilisation. Objects have the power to transform society. This was really interesting to me, to acknowledge that our definition of ‘technology’ is not universal and is attributed by the individual, else it can be debated that anything and everything is technology.

In addition to microtechnologies, there was a discussion on micro-behaviours – how do our simple, everyday activities affect the world on a global scale. In a way, everything we do and every choice we make is political – how we treat ourselves, each other, and our space. These discussions were very valuable to me, as it continues to reinforce my responsibility as a designer to make these considerations on not just a material level, but also a global level.

[Unfolding the Political Capacities of Design](https://www.researchgate.net/profile/Fernando_Dominguez_Rubio/publication/265467472_Unfolding_the_political_capacities_of_design/links/540f69d70cf2f2b29a3dde20/Unfolding-the-political-capacities-of-design.pdf)  
[Discipline and Punish](https://www.goodreads.com/book/show/80369.Discipline_and_Punish)  
[Design Nature Built Environment](https://www.amazon.es/Cosmopolitical-Design-Nature-Built-Environment/dp/1472452259)  
[The Fifteen Year old Climate Activist Who is Demanding a new kind of Politics](https://www.newyorker.com/news/our-columnists/the-fifteen-year-old-climate-activist-who-is-demanding-a-new-kind-of-politics)  
[Name](https://www.cc.gatech.edu/~beki/cs4001/Winner.pdf)    


![]({{site.baseurl}}/nu-eye.gif)
## Mariana Quintero + Tomas Diez

Mariana presented her talk, ‘read-write technologies’, on the process of information exchange and the role of interfaces in today’s society.

The dialogue spanned an array of different topics, but was primarily concerned with language, interfaces, information, and knowledge, contextualized by discussing from a historical/micro perspective, into the present day.

‘Read-Write technologies as the process of exchange of information’, and the interfaces that enable such a process. All physical systems are dependent on such exchange. The argument is that this process changes the course of civilisations.

For example, the human body as an interface – the first component being the self, and the second being the information that surrounds us. Within the body, there are many interfaces, the first example was the eyes – how does the way we receive and encode information into our minds dictate the course of civilisation? What if we had x-ray vision instead of regular sight, how would that affect the cultural notions of privacy, nudity, limits, and boundaries? What will we make of division? The intention was to ‘cultivate doubt’ in our minds through this minor change in existing ‘interfaces’ that would create a rippling effect throughout all of this speculated society. It is argued that culture is dependent on interface.

How we evolve, interface with our environments, and our dependence on ‘the visible’ was questioned throughout this speculation. ‘Scientific truth is a social construct’ was another statement thrown in to provoke debate.

[Jeremy England](https://en.wikipedia.org/wiki/Jeremy_England)  
[Neil Harbisson](https://en.wikipedia.org/wiki/Neil_Harbisson)  
[Technics & Civilization by Lewis Mumford](https://en.wikipedia.org/wiki/Technics_and_Civilization)  
[Three Body Problem](https://www.goodreads.com/book/show/20518872-the-three-body-problem)  
[21 lessons for 21st century](Link)  
[Cyborg Arts](https://www.cyborgarts.com/)  
[Contact](https://en.wikipedia.org/wiki/Contact_(1997_American_film))  
[Voyager Golden Record](https://en.m.wikipedia.org/wiki/Voyager_Golden_Record)  
[The Arrival](Link)  
[Sonar Calling](https://www.sonarcalling.com/)  
[Print the Legend](https://en.m.wikipedia.org/wiki/Print_the_Legend)  
[I Listen to Color](https://www.ted.com/talks/neil_harbisson_i_listen_to_color?utm_source=whatsapp&utm_medium=social&utm_campaign=tedspread)  
[Cryptocurrencies: Last Week Tonight with John Oliver](https://www.youtube.com/watch?v=g6iDZspbRMg)   



# Reflection

We began our reflection module with Oscar, Tomas, and Mariana to reflect on the previous days and decompress our thoughts for the week via group discussion and various modes of reflection. How has my vision changed, and how may I acquire the skills to enact this vision?


### How has my vision evolved?
We iterate and evolve our identity, our intentions, alongside our designs. To define how my vision has evolved I have to position myself, analyse my vision, and compare and contrast with last week’s vision. Within a span of a week, my core values haven’t changed much, but to cite last week’s vision –

> “Create work that seeks to ask questions, provoke, get people thinking about themselves and their surroundings in natural, material, and virtual worlds.”

This week helped engage me to the ongoing dialogues with these worlds. A few ‘why’ loops was asked during our discussions and my reasonings were to “Encourage curiosity and creativity amongst people. (Another layer: provoke abstract thought)”, and again, to “get people thinking in a future forward manner” as a means to encourage collaboration between citizens and technology; empower instead of substitute skills. To accelerate design, digital, technological, and visual literacy, and simultaneously create connections that resonate with our own personal narratives.

For my project, one of the key questions is ‘How is the city’s physical imprint tied to our identities’, from this, it may act as a basis on defining my first prototype in ‘Living with Ideas’. I'm also interested in the idea of 'assisted creativity', and where AI can play a part in it.

Another thread to follow is 'community curation', which would ideally perpetuate multicultural and inclusive creativity, by means of many perspectives. I’d like to design not just functionality, but with experience, poetry, humour. My vision teeters somewhere around the idea of a living, breathing, repository.

Cities and communities are my focal points at the moment, alongside the practices of preservation, archiving, and exhibiting, so Jose’s line of work attracted me.

When talking about museums and institutions, I’m interested in unpacking the colonial history of such spaces, where they seem to not welcome the cultures they display. How does our past inform our present and future, how our present can be preserved and collected for the future also, how to play with the idea of time. How do the debates regarding provenance, diversity, representation, and culture, shape institutional practices, and how can this practice be brought to an urban scale by means of emergent technologies. Another key point that I connected with was the discussion about time, ‘The Now’, and how we must design for the present but also the future, as our impact on the world is a constant.  

Hearing from Pau’s talk on the politics of materiality helps me think critically about navigating the micro vs macro politics when designing in the context of what could potentially be on multiple scales.

Potential scales of interventions

* Product
* Experiential
* Participatory
* Systems
* Repository/Network

Mariana’s talk also resonated deeply with me by unpacking the definition of ‘interfaces’, I think it’s let me think about interfaces in a more global manner and broadens my scope of thinking. Designing for and with the senses, symbols, and linguistics, as a means to deepen modes of communication is something I hold in high regard, in conjunction with constant critique of the design process. Speaking in abstracts whilst referring to history and actual applicable projects helped me process this talk a lot better than the previous two, as it combined theory with application.

In one sentence, my vision is to design experiences that encourages engagement and instills curiosity to, accelerate digital/technological literacy.  


### Abstraction and Application  
To bridge between abstraction and application, I’ve documented some initial findings to try align my values to existing people and projects.

[‘Insert___Here’](http://www.evemosher.com/2011/insert-____-here-with-350-org-2011/) by Eve Mosher “put(s) the power of creative thinking in the hands of community organizations and give people a chance to think positively in the face of climate change.” There is a lot of elegance to the simplicity of a simple call to action placed in our space. Similarly, another project where she collaborated with Renata Mann resulted in ['(Intra)Structure'](http://www.evemosher.com/2011/intrastructure/), combining personal and urban scale modes of interventions. These mix of scales is what attracts me to these pieces.

![]({{site.baseurl}}/nu-jenny-holzer.gif)

[Jenny Holzer](https://projects.jennyholzer.com/projections) is a conceptual artist that often works with text-based interventions to the public space. Her use of typography within the urban space, to make political statements via modern information systems creates a really interesting intersection in the spaces I would like to explore.

A quote by Jenny Holzer:

> “I used language because I wanted to offer content that people—not necessarily art people—could understand”

![]({{site.baseurl}}/nu-bench.jpg)
['Bench Press'](http://brolab.org/bench-press/) by BroLabs, a portable commuter bench. It reminds me of our 'modular station', in an urban context.

![]({{site.baseurl}}/nu-hello-lampost.jpg)
['Hello Lamp Post'](https://www.playablecity.com/projects/hello-lamp-post/) is an invitation for people to strike up a conversation with city objects. It increases our awareness of our surroundings in a playful, unconventional manner.

![]({{site.baseurl}}/nu-human-inc.jpg)
['Human Element Inc'](https://philippschmitt.com/work/human-element) is a speculative crowdwork service product.

![]({{site.baseurl}}/nu-computed-curation.jpg)
['Computed Curation'](https://philippschmitt.com/work/computed-curation) is a photobook created by a computer. How may we collaborate with AI when we speak of creativity and content creation?

![]({{site.baseurl}}/nu-terrapattern.jpg)
['Terrapattern'](http://www.terrapattern.com/), a visual search tool for satellites. Patterns, symbols, oddities.

![]({{site.baseurl}}/nu-ml.jpg)
['ml5: Friendly Open Source Machine Learning Library for the Web
'](https://itp.nyu.edu/adjacent/issue-3/ml5-friendly-open-source-machine-learning-library-for-the-web/)

![]({{site.baseurl}}/nu-sougwen.jpg)

['Omnia per Omnia'](http://sougwen.com/project/omniaperomnia) reimagines the tradition of landscape painting as a collaboration between an artist, a robotic swarm, and the dynamic flow of a city.

![]({{site.baseurl}}/nu-transient-spaces.jpg)
The most recent project of interest to me is [Transient Spaces](https://www.itsnicethat.com/articles/transient-space-disrupt-the-channel-digital-191018), a ‘public gallery in a non-space’, with the term non-space being derived from French anthropologist Marc Augé, referring to “human spaces of transience where people remain anonymous, where the spaces do not hold enough importance to be deemed real ‘places’.”

Examples shared in class:

* [El Sistema](https://fundamusical.org.ve/el-sistema/#.W-Adz3pKjOQ)
* [Aqua Pioneers](http://aquapioneers.io/)
* [Precious Plastics](https://preciousplastic.com/)

Ending off with a ['Social Allegory'](http://francisalys.com/when-faith-moves-mountains) by Francis Alys


>“Faith Moves Mountains is my attempt to deromanticize Land art.”

<iframe src="https://player.vimeo.com/video/130921402?color=d8d5d4" width="640" height="458" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/130921402">When Faith Moves Mountains</a> from <a href="https://vimeo.com/francisalysvideos">Francis Al&yuml;s</a> on <a href="https://vimeo.com">Vimeo</a>.</p>


# Fields of Interest

Reads and to-reads into categories that I’ve been engaging with alongside that of MDEF to further narrow down the fields of interest.

##### Read   

![]({{site.baseurl}}/nu-books.jpg)

* The Politics of Design
* Design Fiction Vol 2
* The Future of Public Space
* E-flux Journal Reader

Design  
[Collective Intelligence in Design](https://arena-attachments.s3.amazonaws.com/2027096/fda93644ffdb3cffc1c34c61ebedc6c8.pdf?1523438813)  
[Black Gooey Universe](http://unbag.net/issue-2-end/black-gooey-universe/)    

Web   
[A Vernacular Web](http://art.teleportacia.org/observation/vernacular/)    
[The Web's Grain](https://frankchimero.com/writing/the-webs-grain/)    

AI
[FutureCrafting: A Speculative Method for an Imaginative AI](https://drive.google.com/file/d/15d3olFW7n6LICiBLaw4UWki7Zkdacnyp/view?usp=sharing)  
[How Anticipatory Design Will Challenge Our Relationship with Technology](https://drive.google.com/file/d/1rZ2gwuh2SSBoyuho059VJ3rZxb9Mwm3p/view?usp=sharing)   

Space
[Leverage Points: Places to Intervene in a System](http://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/)  
[Societal Innovation & Our Future Cities](https://provocations.darkmatterlabs.org/the-societal-contract-for-innovation-15593ae9a1d4)  
[Modern Man in the Making](https://www.amazon.com/Modern-Man-Making-Otto-Neurath/dp/B00085D3DI)  

Archive   
[The Free Library and Other Histories](https://annmessner.net/wp-content/uploads/2018/02/the-library-and-other-histories_ann-messner_2018.pdf)  
[The Suspicious Archive, Part I: A Prejudiced Interpretation of the Interpretation of Archives
](https://www.e-flux.com/journal/75/67172/the-suspicious-archive-part-i-a-prejudiced-interpretation-of-the-interpretation-of-archives/)   
[An Archival Impulse](https://arena-attachments.s3.amazonaws.com/2319500/0fc28cd630c2a3b410f456b84571453f.pdf?1529165855)  
[Art and GLAM: Promoting Creative Uses of Archives
](https://itp.nyu.edu/adjacent/issue-3)   
[The Invisible Collection](https://www.documenta14.de/en/south/36_the_invisible_collection)   


Art/Curation   
[Curating in the Post Internet Age](https://www.e-flux.com/journal/94/219462/curating-in-the-post-internet-age/)  

Philosophy    
[Our Aesthetic Categories: An Interview with Sianne Ngai](http://www.cabinetmagazine.org/issues/43/jasper_ngai.php)   
[The Work of Art in the Age of Mechanical Reproduction](https://www.marxists.org/reference/subject/philosophy/works/ge/benjamin.htm)  
[The Weirdest People in the World?](https://arena-attachments.s3.amazonaws.com/2561096/452f2bdcb7659bcdf056619fef80320a.pdf?1534373094)    

Type/Language   
[Introduction to Electronic Literature](http://dsnelson.bol.ucla.edu/~elit/2017.html)   
[Words as Material](https://www.nicolefenton.com/words-as-material/)   
[The Crystal Goblet, or Printing Should Be Invisible by Beatrice Warde](http://ci.nikasimovich.com/{{site.baseurl}}/readings/warde-beatrice_the-crystal-goblet.pdf)   
[A Man of Letters](https://www.newyorker.com/magazine/2010/06/28/a-man-of-letters-2)    
[Glass, Irony and God](http://s3.amazonaws.com/arena-attachments/2558953/d7e1f94c0073ce187c166e8847bef503.pdf?1534349638)    
[Typophoto](http://ci.nikasimovich.com/{{site.baseurl}}/readings/moholy-nagy-laszlo-typophoto.pdf)    


Other   
[The IKEA Humans: The Social Base of Contemporary Liberalism](https://jacobitemag.com/2017/09/13/the-ikea-humans-the-social-base-of-contemporary-liberalism/)   
[All Technology is Assistive](https://medium.com/backchannel/all-technology-is-assistive-ac9f7183c8cd)       
[In Defense of the Poor Image](https://www.e-flux.com/journal/10/61362/in-defense-of-the-poor-image/)   
[Fuck Content](https://2x4.org/ideas/2/fuck-content/)  


##### Watch  

[Jacopo Galimberti, "The Worker, the Militant, and the Monster: Visualizing Revolutionary Subjectivities in 1960s and 1970s Italy"
](https://www.e-flux.com/video/197791/e-flux-lectures-jacopo-galimberti-the-worker-the-militant-and-the-monster-visualizing-revolutionary-subjectivities-in-1960s-and-1970s-italy/)   
["After Midnight: Fast Forward Art History," a panel with Molly Nesbit, Hilton Als, Yasmin Ramirez, and Ann Reynolds](https://www.e-flux.com/video/179490/after-midnight-fast-forward-art-history-a-panel-with-molly-nesbit-hilton-als-yasmin-ramirez-and-ann-reynolds/)  
[Dimensions of Citizenship](https://www.e-flux.com/video/192062/dimensions-of-citizenship-roundtable-presented-by-the-us-pavilion-at-the-venice-architecture-biennale-nbsp-2018-and-nbsp-e-flux-architecture/)  
[David Rudnick. Lecture "Crisis of Graphic Practices: Challenges of the Next Decades"
](https://www.youtube.com/watch?time_continue=303&v=-ejp4AvetSA)  
[In Praise of Chairs](https://www.youtube.com/watch?v=FfGKNJ4mldE)  

##### Library   

[E-Flux](https://www.e-flux.com/)  
[Graphic Design Readings](http://readings.design/)  
[Metadata Archive](https://www.are.na/daniel-pianetti/metadata-archive-references)   
[Nika Simovich & Dylan Fisher Readings](http://ci.nikasimovich.com/readings/)   
[Open Library](https://openlibrary.org/)  
[Ubu Web](http://ubuweb.com/)  


---

# Links and Resources

---

[Humanizing the Singularity](https://medium.com/humanizing-the-singularity/why-global-trade-will-inevitably-move-to-the-blockchain-ab3f66bd20f8)  
[Blockchains - Killer app making trade wars obselete](https://www.coindesk.com/blockchains-killer-app-making-trade-wars-obsolete/)  
[What Blockchain means for the Sharing Economy](https://hbr.org/2017/03/what-blockchain-means-for-the-sharing-economy)
[The Societal Contract for Innovation](https://provocations.darkmatterlabs.org/the-societal-contract-for-innovation-15593ae9a1d4)  
[The New Threat to Liveral Democracy](https://www.theguardian.com/books/2018/sep/14/yuval-noah-harari-the-new-threat-to-liberal-democracy)  
[El Sistema](https://fundamusical.org.ve/el-sistema/#.W-Adz3pKjOQ)  
[Plethora Project](https://www.plethora-project.com/education)
[Consul Project](http://consulproject.org/en/)    
[Citizen Lab](https://www.citizenlab.co/)  
