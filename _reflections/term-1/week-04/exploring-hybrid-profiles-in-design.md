---
number: 04
title: Exploring Hybrid Profiles in Design
period: 22-28 October 2018
date: 2018-10-28 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/hybrid-intro-loop.gif)

---


![]({{site.baseurl}}/hybrid-dds-loop.gif)

## [Domestic Data Streamers](https://domesticstreamers.com/ "DDS")  

Domestic Data Streamers ‘create meaningful connections between information and people’. They aim to reach wide audiences differently by illustrating data as both human and digital. Through mixing the disciplines of science, design, engineering, and more, they create experiences with numbers and patterns through an abstract, but approachable way. DDS use design methodologies to understand communication, in all different perspectives, to understand human interaction and gain knowledge to create better experiences.

![]({{site.baseurl}}/hybrid-dds-studio.jpg)

The case studies they presented included:

* [GA at the United Nations
](http://domesticstreamers.com/case-study/general-assembly-united-nations/  
 "GA at the United Nations")   
* [Design Does* Exhibition
](http://domesticstreamers.com/case-study/design-does/  
 "DD")  
*  [The Timekeeper for Spotify
](http://domesticstreamers.com/case-study/the-timekeeper-spotify-data-analytics/
 "Timekeeper")   
* Mood Visualization over 24 hours    
* Data Strings
* Telefonica R&D – Radio Me   



I liked their mantra of making ‘visible what’s invisible’, as demonstrated through our practical with them. It allowed for an intimate look into our daily lives in such a simple manner, as we visualized our own data through our objects and opened up habits, mannerisms, quirks, anxieties, and processes. It felt introspective in a way that I am not normally in regards to my material belongings, and let out stories when you normally wouldn’t day to day.

Domestic Data Streamer’s emotional yet strategic approach to the design process appeals to me, as they have to balance creativity with a rigid management within timing, budgets, clients, etc. I really like that as an agency they seem to be medium agnostic, creating installations or apps or exhibitions depending on the needs of the client.

I feel that they do that designers do best; synthesize and distill information to an audience in a concise, delightful manner. Creating systems within their art, and seeing the public take pleasure in the contribution also felt like like a very design-based approach to their projects.

![]({{site.baseurl}}/hybrid-dds-studio-2.jpg)

It was exciting to see the experiential data mixed with physical interaction; not just creating data visualizations out of CSV files – they’re creating real-time data through participation and collaboration. Noting the ethics around data and collection of data was also an important part of the dialogue when it came to DDS – remaining responsible with the process of collection, biases, privacy is something to be diligent with.

The trajectory of DDS, from conceptual installations to larger, more commercial work as an agency was also very interesting. The participatory aspect of their approach to managing and collecting data is a large part of my interest in them, to be able to combine physical and computational interaction in an approachable and playful manner. I like the emotive aspect and acknowledgement of the more unconventional way of approaching data, as it’s less cold, and more rooted in social sciences.

----

![]({{site.baseurl}}/hybrid-ariel-loop.gif)

## [Ariel Guersenzvaig](https://twitter.com/interacciones?lang=en "Ariel")  

Ariel spans UX + Research at Elisava, mixing design and ethics within an academic context. His expertise originated from the design and development world of the web, to design ethics, decision making, publishing, and more. Over the years he was able to adapt to the rapid technological developments alongside his personal design trajectory through working within the fields of design, management, teaching, and research.  

![]({{site.baseurl}}/hybrid-ariel-eli.jpg)

I related a lot to Ariel’s background as my own is within digital design. I like the intersection of practitioner and researcher in his trajectory, they continuously cross throughout his life but it is rooted within the design discipline, and that is how I would like to work– in service of design. Being a lifelong student, and simultaneously a teacher, is something I hope to integrate into my life too; I value the democratizing of knowledge and hope to be able to pass on information as much as I can. A hurdle to jump over for me would be to develop the confidence in teaching and sharing knowledge.

Ariel also discussed with us his views on the design process and design decision making, dissecting those areas to see how designers really work; how designers live in the realm of the future, to ‘simulate how the future will use the product/space’. He identified a difference between designers and engineers’ processes of decision making, how the latter would consider and assign value to each variable to pick the best, most optimal solution, in comparison to designers who work more holistically and are dependent on intuition.

Another key point for me was the debunking of an ‘isolated phase of analyse’ within the design process, which I found to be very true from experience, the typical design process of research first and design later does not exist. He then led us into a conversation around design intuition, how it is built and developed, and the dangers of an uninformed intuition that isn’t grounded in experience.

![]({{site.baseurl}}/hybrid-ariel-dreyfus.png)

We were subsequently introduced to the ‘Dreyfus Model’ of expertise, which states that the more expertise you hold, the less dependent on rigid processes you will be, and will be able to attain a greater relevant focus combined with your intuition.  

It was good to re-introduce myself to the world of the web within a critical and academic context, and fill in some gaps of cultural/historical information. I feel the need to be more meticulous about the history of my profession, as it was something I dove into very quickly. When I worked, I was informed by the client context and business constraints, now I hope to add the layers of decision making, ethics, and have a more future focused, consequence aware approach to all design decisions made. The values of ethics, sustainability, and usability are ones for me to constantly be aware of.

---

![]({{site.baseurl}}/hybrid-sara-loop.gif)

## [Sara De Ubieta](http://www.deubieta.com/lang=en"Sara")

Sarah is an architecturally trained shoemaker, who transitioned when she realized her true passion, and now works within design design/production, research, art, and teaching, through her collaborations with museums, arts/curators, universities, as well as working with her own studio.

She naturally combines the two fields and specialities, to find material opportunities informed by her knowledge of architecture. Pulling certain tropes from architecture into shoe-making was very compelling and created some poetic pieces of work, such as in her shoes created out of the material seen on pavements, soles made of wooden beams, and the use of leather scraps as the main material of a shoe.

![]({{site.baseurl}}/hybrid-sara-studio.jpg)

Neither of Sara’s fields relate to me – architecture and shoe-making. But I related a lot to her approach to work and ethos. Her natural passion and enthusiasm was magnetic to us and we felt shared sense of joy during our time with her. I like the experimentation that is rooted in design decisions, and passions she brought with her from architecture. The intersection of research, design, and art, really epitomizes the space I see myself in. I’m finding that I really like the opinions of ex-architects, their ability to extract their knowledge of the built environment into other fields gives them a unique perspective on materials, forms, and structures.

Personally, I’ve also been trying to wean off fast fashion in my life, and seeing the artistry she displayed has motivated me to stay a path of seeking out interesting pieces, re-used clothing, etc, and maintain diligent on what I decide to purchase.

![]({{site.baseurl}}/hybrid-sara-prac.jpg)

I’ve been finding that a gap in my thinking is centred around materiality, how to think about materials in a sustainable and optimal way. A stronger understanding of materials is something I’d like to work on developing throughout this masters. The workshop that followed consisted of us getting into groups and creating our own shoes, we were subsequently very excited, dived deep, and were very proud of our shoes. Ours was created by housing the material in a single piece of string doused in glue.

I was also reminded of a few other practitioners when exposed to Sara’s work – [Nicole McLaughlin](https://www.instagram.com/nicolemclaughlin/lang=en"Nicole"), [Alexandra Hackett](http://www.a-l-c-h.com/"Alch"), and [Arti](https://www.instagram.com/_artwerkz/
"Arti")

----

![]({{site.baseurl}}/hybrid-skill-knowledge-attitude.png)

## Identity

The workshop for Thursday with Oscar consisted of three phases

* Identifying things we liked about the previous days (and how it is tied with us)
* Mapping those points into our current and future selves
* Personal development plan via the 12 weeks of the first trimester

Alongside the discussion of what we found most relatable, Oscar also brought up other people’s methods of reflecting/documenting – via a matrix, looking at the week as a whole, through the lens of themes/connections, etc. I like that we embrace our own uniqueness and modes of thinking in this course, and champion the differences in how we approach problem solving.

### Key Highlights

For me, my key points that I identified were:

* _Divided their time between different practices_ – intersected design, research, and technology. Ability to traverse different areas
* _Not restricted by medium_ – worked in public spaces/installations/apps/anything
* Collaboration/creation of and with arts/curation/exhibitions
* _Researchers_ – inquisitive, lifelong students
* _Deconstruction of processes_ – ‘Technique is not the be all and end all’ from Sara, DDM’s slow integration of disciplines into their business, Design decision making/rationality/processes with Ariel  
* _Deconstruction of themselves, and their techniques_ – looked within for emotive solutions and were naturally guided by their interests
* _Responsibility_ – design to be responsible to ourselves, our environment, our future

### Present and future selves

We then separated those key points into our ‘present’ and ‘future’ selves skills/knowledge/attitudes, we already have, and what we would like to develop

<table>
  <tr>
    <th>I am</th>
    <th>I want to</th>
  </tr>
  <tr>
    <td>
    <ul>
    <li>Open/playful with the design process </li>
		<li>Familiar with reframing/opening up/creating questions</li>
  		<li>Aware of design methodologies and processes</li>
  		<li>Depending on context, can be methodological or improvisational</li>
  		<li>Ready to expect the unexpected</li>
	</ul>  
</td>
    <td>
        <ul>
		<li>Not be restricted by medium</li>
		<li>Develop deeper research skills to inform my work</li>
  		<li>Learn how to make things</li>
  		<li>Intersect design with teaching, making, and researching</li>
  		<li>Gain greater knowledge in tech, development, cultural/historical/economic sectors</li>
  		<li>Improve verbal teaching/explaining/presenting skills, to articulate myself thoroughly, confidence in debates</li>
		<li>Communicate larger ideas</li>
  		<li>Think spatially</li>
		</ul>
</td>
  </tr>
  </table>

### Personal Development    

Finally, we projected those wanted learnings into the MDEF timeline to create a personal development plan. We were also encouraged to consider what we would like to learn that isn’t already in the MDEF schedule. For me, it would be teaching skills, facilitating community projects, and some introduction on AR/VR technologies.

<table>
  <tr>
    <th>#</th>
    <th>Title</th>
    <th>Learning Goals</th>
  </tr>
  <tr>
    <td>1</td>
    <td>MDEF Bootcamp</td>
    <td>Learn about our space through – my peers, our digital environment (GIT), physical environment (classroom, Poblenou).</td>    
  </tr>
  <tr>
    <td>2</td>
    <td>Biology Zero</td>
    <td>Develop further understanding of the natural world, contextualized by design and philosophy</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Design for the Real Digital World</td>
    <td>Work within digital fabrication, manual labour, and a multidisciplinary team.</td>
  </tr>
  <tr>
    <td>4</td>
    <td>Exploring Hybrid Profiles in Design</td>
    <td>Intersect design with further disciplines – primarily in arts, teaching, researching.</td>
  </tr>
  <tr>
    <td>5</td>
    <td>Navigating Uncertainty</td>
    <td>Develop ability to research more deeply with an awareness of 		historical/cultural/political context of design decisions. Develop 		more informed baseline when discussing design.</td>
  </tr>
  <tr>
    <td>6</td>
    <td>Designing with Extended Intelligence</td>
    <td>Create a baseline knowledge for working with AI + Data – How it 	works, ethics, practical applications, how to develop said practical  		applications.How can this inform visual experiences?
	Learn about the impact it will have on the future.
	How can we design with AI in mind? Learn about implications, 			consequences, and potential.</td>
  </tr>
    <tr>
    <td>7</td>
    <td>The Way Things Work</td>
    <td>Learn making skills</td>
  </tr>
  <tr>
    <td>8</td>
    <td>Living with Ideas</td>
    <td> Building further my personal narrative, development, and footprint. Increase awareness of my impact on the environment. Better my understanding of how my character and biases will inform my outputs </td>
  </tr>
  <tr>
    <td>9</td>
    <td>Engaging Narratives</td>
    <td>Embrace abstraction and work within the emotive, human, undefined thoughts</td>
  </tr>
  <tr>
    <td>10</td>
    <td>An Introduction to Futures</td>
    <td>Develop better understanding of design futures, speculative design, design fictions</td>
  </tr>
  <tr>
    <td>11</td>
    <td>From Bits to Atoms</td>
    <td>tbc</td>
  </tr>
  <tr>
    <td>12</td>
    <td>Design Dialogues</td>
    <td>Define my direction for the rest of the masters</td>
  </tr>
</table>

---

## Vision
To create our vision, we must find direction. Oscar broke down three pathways to find that direction through analysing vision, reflection, and experience.

The three articles presented were:

* [Vision: Designing for the Unknown](https://www.researchgate.net/publication/254941682_Designing_for_the_unknown_a_design_process_for_the_future_generation_of_highly_interactive_systems_and_products "DDS")   
* [Reflection: Annotated Portfolios and other forms of intermediate-level knowledge](http://interactions.acm.org/archive/view/january-february-2013/annotated-portfolios-and-other-forms-of-intermediate-level-knowledge "DDS")   
* [Experience: Designing for, with or within: 1st, 2nd and 3rd person points of view on designing for systems](http://interactions.acm.org/archive/view/january-february-2013/annotated-portfolios-and-other-forms-of-intermediate-level-knowledge
 "DDS")   

The collated takeaways per article were compiled in a table

<table>
  <tr>
    <th>Vision</th>
    <th>Reflection</th>
    <th>Experience</th>
  </tr>
  <tr>
    <td>Learn from mistakes<br>
Gather data<br>
Build vision<br>
Dialogue<br>
Jump from phases<br>
Speculate<br>
Integrate approaches<br>
Flexibility<br>
</td>
    <td>Annotations<br>
Exemplars<br>
Guidelines<br>
Critiques<br>
Tacit to Explicit<br>
Tools and Methods<br>
</td>
    <td>Breaking barriers<br>
Stakeholder designer<br>
Motivations<br>
Not imposing<br>
Openness<br>
Flexibility<br>
Being in the real<br>
Making transformations<br>
Capabilities<br>
Infrastructure<br>
</td>    
  </tr>

</table>


I read the third article, ‘Experience: Designing for, with or within: 1st, 2nd and 3rd person points of view on designing for systems’

My key takeaways were:

* A reminder to be aware of my own biases when doing research
* Not make assumptions and impose unneeded products on users
* General/desk research has its place, however more is needed.
* Vice versa; the ‘first person’ integration was highly practical was not enough to be a well-rounded process.  

### What is a design vision?

As brainstormed by the class:

* Prediction
* Goal
* Path
* Driving force
* Scenario
* Authentic
* Framework
* Coherent thought
* Aspiration
* Commitment

I think before determining my vision, I wanted to also consider my values, in addition to the specified skills/knowledge/attitude. My values lie within honesty, transparency, creativity, curiosity and freedom. How will this inform the output of my work?

![]({{site.baseurl}}/hybrid-veronica-loop.gif)

## A Draft Vision
I wish to design interactions that seek to ask questions, critique, explore, and experiment in the hopes of sparking inspiration within our natural, built, and virtual environments.

Through this, I can seek to provide clarity from abstractions, democratize knowledge, question trends/cultures, find insights on our feelings, habits, needs/wants, employ playful interventions to our daily lives. I aim to communicate information succinctly, and distill key points in a visual manner to encourage curiosity and empower citizens through the means of emergent technologies.

Further questions to actualize this would be

* What would be the form of interaction - platform, product, participatory, experiential, etc?   
* What can we all learn from each other?
* What is my role in design, materials, and systems?
* What problems do I hope to solve?

Over the course of the masters, I intend on narrowing down and iterate on the draft to continue working towards a clearer vision of myself.

## Reflection

I didn’t expect to connect with all three practitioners that much, and assumed DDS was closest to where I would want to be. I feel like this week has allowed me to open my mind on previous restrictions I’ve set for myself because of my current skill-set. As of now I see design being my core for a long time, but I feel like secondary focuses and being open to opportunities within teaching, speaking, and further collaborations feel much more tangible now. I learnt that I don’t have to be either researcher or practitioner, as evident in all three profiles this week. There is definitely room for both, and learnt that you can imbue your personal and professional life with as much inspiration as you want.

My key areas for focus would be design, creative direction, research, and art. As secondary focuses, I would love to dip my toes into teaching, managing, crafting. One of the biggest reasons I am a designer is because we have the ability to collaborate with anyone, and this week’s practitioners only reinforced that.  

Some of my weaknesses lie in lack of knowledge within consequences/implications – I don’t feel informed enough beyond intuition to confidently make ‘big’ decisions within economic/political contexts. Design is inherently political, so I see this as a huge flaw that can be rectified with thorough research. Areas I’m excited to and have begun to delve into are web development and digital fabrication.

## Links and Resources

#### Articles for Finding Direction
* [Vision: Designing for the Unknown](https://www.researchgate.net/publication/254941682_Designing_for_the_unknown_a_design_process_for_the_future_generation_of_highly_interactive_systems_and_products "DDS")   
* [Reflection: Annotated Portfolios and other forms of intermediate-level knowledge](http://interactions.acm.org/archive/view/january-february-2013/annotated-portfolios-and-other-forms-of-intermediate-level-knowledge "DDS")   
* [Experience: Designing for, with or within: 1st, 2nd and 3rd person points of view on designing for systems](http://interactions.acm.org/archive/view/january-february-2013/annotated-portfolios-and-other-forms-of-intermediate-level-knowledge
 "DDS")   

#### Ariel’s Topics
* Ethics and Philosophy, to reflect on Design and Technology
* Publishing and Human-Computer Interaction
* ‘Writing for the Web’ – Cognitive aspect of text on screen; text connected through hyperlinks, cognitive aspect of text on screen vs on paper
* ‘Design Decision Making’
* ‘Design Rationality’
* ‘Design Intuition’ – How designers make decisions
* ‘Ethics of Designing’
* ‘Killers Robots’

#### Ariel’s Recs/Books/Principles

* [Information Architecture for the WWW](https://www.amazon.com/Information-Architecture-World-Wide-Web/dp/0596527349 "WWW")
* [‘Interactions’ Magazine](http://interactions.acm.org/ "Interactions")   
* [Philosophy of Design](https://en.wikipedia.org/wiki/Philosophy_of_design)   
* [The Design of Everyday Things](https://www.amazon.com/Design-Everyday-Things-Donald-Norman/dp/1452654123)  
* [Affordances in HCI](https://www.interaction-design.org/literature/topics/affordances)   
* [The Inmates are Running the Asylum](https://www.amazon.com/Inmates-Are-Running-Asylum-Products/dp/0672326140)  
* [Dreyfus Model](https://en.wikipedia.org/wiki/Dreyfus_model_of_skill_acquisition)   
* [Alasdair MacIntyre](https://en.wikipedia.org/wiki/Alasdair_MacIntyre)  
* [The Capabilities Approach](https://plato.stanford.edu/entries/capability-approach/)   
* [The Capabilities Approach, Technology, and Design](https://www.springer.com/gp/book/9789400738782)   
* [Ir. Oosterlaken Delft Dissertation](https://ethicsandtechnology.eu/member/oosterlaken_ilse/)  
* [Amartya Sen - Identity and Violence](https://www.amazon.co.uk/Identity-Violence-Illusion-Amartya-Sen/dp/0141027800)   
* [Abebooks](https://www.abebooks.com/)  
* [Humanitarian Law - ‘Just War Theory’](https://www.lrb.co.uk/v04/n04/jonathan-glover/letting-people-die)   
* [‘Ban Killer Robots’Campaign](https://www.stopkillerrobots.org/)  
* [End of Life Robot by Dan Chen](https://www.nextnature.net/2012/06/end-of-life-care-machine/)   
