---
number: 02
title: Biology Zero
period: 8-14 October 2018
date: 2018-10-14 12:00:00
term: 1
published: true
---

# Reflection
‘Biology Zero’ felt like learning about a series of systems within systems, each so complex and
unifying to form everything on this universe.

The parallels between science/design, theory/practical, fablab/biolab, micro/macro,
natural/unnatural, life/death, etc. were particularly interesting to me as it allows us to compare
and contrast our processes/information, potentially making interesting connections where you
wouldn’t otherwise. Hence it helped me align and reconfigure my own values/methods within my
practice, digest the information in a tangible manner, and draw some lines between the
concepts to aid my understanding of it. A key word in my learning process for the week would
be ‘connection’ - trying to draw lines between all the key topics helped me process the information moe efficiently.

The ongoing dialogue between the fields of science, design, technology, and philosophy felt
somehow unifying as we spoke of humanity and the world as a whole. Starting from the
sub-atomic level of bacteria right through to larger ideas of culture and consciousness, and into
human interventions to nature. I was able to learn the basic concepts of theoretical biology,
alongside current technological advances in synthetic biology, in conjunction with its connection
to design via the conversations around materials and FabLabs.

In addition, seeing how biology is intertwined with design through the works of Mette Bak-Anderson, Thora H. Arnardottir, and Jessica Dias were hugely inspiring and acts as a great reference for future research.

Below is a pathway of my key learnings throughout the week, in an attempt to distill and simplify to just the most stripped back raw data. It acts as a reference to the idea of accessibility in knowledge and learning, and stylistically, is a more interpretive rendition of scientific diagrams.

![]({{site.baseurl}}/bio-zero-illustration-edge.svg)


# Designing an Experiment
## Hypothesis
Mathematical models/programmatic patterns can be derived from the geometric code of
spiderwebs, and will be used to inform smart structures/materials, matrices, and more.
## Method
1. Observe and record patterns, such as entangled/deformations within the webs
2. Observe process of creation of the webs
3. Properties would respond to certain effects, such as pushing, pulling, stretching, tearing.
Test the damage displayed
4. Elemental variables - Test reactions in a variety of controlled temperatures
5. Decompose and test the material
6. Pattern recognition -- Recognise patterns and variables displayed
7. Design and iterate potential mathematical models based on observations

## Potential Directions
- Simulate web's patterns to optimize networked systems; recognition algorithms,
mobility/transport patterns, interfaces and communications. Explore the mechanical vs natural
systems.
- Observe tensile structure to apply in rope structures within boats - cord geometry
- Nets in relation to garments and materials. Explore permanency and preservation.
