---
number: 01
title: MDEF Bootcamp
period: 1-7 October 2018
date: 2018-10-07 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/01-mdef-bootcamp-learning.gif)
## What did I learn?
### Intro
During the initial introductions, I resonated most with the following points, and want to continue thinking of ways to embed this dialogue into my practice

- Wanting to be sharing knowledge and learning constantly
- Conscientious of both old and new techniques and ways of making, how to bridge - and deconstruct processes
- Design process being driven from the needs of people
- Participatory design, and the acknowledgement of technology being intertwined with class and gender

I feel inundated with information, so I learned to lean into the madness, as I feel there’s a method and overarching ideology that helps us define our direction through iteration. I learned that we will be actionable and strategic in our approach to intervening and designing for emergent futures, as demonstrated in the focus on goals + 10 year visions. It feels like a privilege to be able to collaborate with an immensely diverse cohort.

I hadn’t considered the notion of not knowing what I want to do because it doesn’t exist, and therefore I must invent – my previous way of thinking was to adapt to new technologies by improving my skill set. I think it became a key point in reframing how I thought about my future, more as a driver than a passenger.

These were the groups I defined after the introductions:   
- Serving communities/people   
- Designing processes  
- Sharing knowledge  
- Environments and sustainability  
- Utilizing emerging technologies for good  
- Decolonizing/democratizing existing patterns

A few further key points I began to collate and think about in unison with my group, ‘Social Cohesion’:

- Collaboration and social responsibility alongside championing efficiency and happiness
- Think about the community but also the individual experience - spaces to live that are positive and affordable - live, work, socialise
- Element of storytelling and culture
- Laboratory and collective experience


### GIT
![]({{site.baseurl}}/02-mdef-bootcamp-GIT.gif)  
I felt a lot of confusion over the execution of the workshop – I think it needed more contextualizing and clarification in the practical workflow, which is why I gained a lot of value after the ‘Documenting your Projects’ lecture with Mariana – summarizing git as the ‘branching, contributing, and pushing to a central repository’, in order to have different ongoing versions of a central idea. I really like the transparency of process and, combined with our own space + the team’s space.

I am extremely excited to see the pushing and pulling, to and from a central nervous system in our work and what mutations can come out of it.
### Poblenou
![]({{site.baseurl}}/03-mdef-bootcamp-poble.jpg)
I see Poblenou in a new light after the tour! The ‘underground’ fab lab network feels so authentic and everyone’s individual obsession was very heartening. Seeing a ‘prototype’ of a maker district that I can walk through feels tangible and impactful, as well as contextualizes our physical space for the coming 9 months. Trash hunting to me was a practical starting point to carry through the dialogue that happened in class; deconstructing the notion of waste; detaching the negative connotations, and truly seeing the city as a repository.

I recognized that the set of tools available is much wider than software on my laptop – my toolbox consists of my body, mind, space, and city.

Places of interest:

* Indissoluble, design, production, supply of museum pieces/temporary spaces  
- Transfolab, investigate and play with, thereby transforming trash  
- Biciclot, bike repairs + education  
- Leka, profitable open source restaurant  

## What is my learning process?
![]({{site.baseurl}}/04-mdef-bootcamp-learning-process.jpg)
It would probably change depending on what I'm learning, but this week, it was via the following steps:

1. **Receiving.** Documenting content, whether by hand or on the computer is essential for me to then process the information. It’s usually filled with shorthand, drawings and quick notes, this allows me to jot down information quickly and allow myself the space to make sense of it later.

2. **Processing.** The majority of the discussions that surrounded this week felt very stimulating, but dense as it covered a variety of different topics I have little knowledge on. I can compare and contrast and categorize it to what I already know, and connect dots during this stage of learning.
3. **Planning.** What will I do with this information? How do I approach the practical component of learning? Here I contemplate and ask questions, to fill in gaps
4. **Actioning.** The most important and most repeated step for me, is doing. Diving into git, scavenging Poblenou, discussing with peers, are all loops to be repeated again and again. Collaborating, discussing, making, etc.
5. **Reflecting.** I think the emphasis on reflection is paramount to me being able to absorb and remember everything.

## Relation to my work
![]({{site.baseurl}}/05-mdef-bootcamp-learning-my-work.jpg)
*My phone decided to splice together 3 images into a panorama*

After the first week, I feel that my process and practice aligns with this masters in the openness – seizing opportunities through improvisations, ‘with and for’ people.

So far it has helped me comprehend and reflect on practices that isn’t communication design, resisting insulating myself in a design bubble, and think more deeply about why I am producing something.

Currently, I’m looking to define what my work is as I learn more – I know that as a communication designer I am concerned with visual literacy, intimacy, and functionality.

I’m interested in organizing and processing systems so that others can receive information in a way that may evoke a connection. I want to design for curiosity and happiness.  I’m also interested in the dialogue between physical and digital, I feel familiar with digital, and foreign to physical, so this week’s tasks on going out and using the body and district as a tool was an exciting start to the impending ‘making’ we’ll commence!

10 years out, I hope to be executing and facilitating design solutions in varying capacities - whether it be process or product driven. In this masters, one primary goal is to experiment and learn as much as possible, and to then build that relevancy into my work.

---

## Some shared links from this week
##### Read
[Speculative Everything](http://readings.design/PDF/speculative-everything.pdf)  
[Adapting to peak oil and climate change
](https://www.dropbox.com/s/4zvfvaujuw6lzep/Dec%2012%2C2011%20Transition.pdf?dl=0)  
[Decolonizing digital fabrication
](https://www.researchgate.net/publication/327133925_decolonizing_digital_fabrication)

##### Watch
[HyperNormalisation 2016
](https://www.youtube.com/watch?v=-fny99f8amM)  
[The century of the self
](https://www.youtube.com/watch?v=eJ3RzGoQC4s)  

##### Resource/Tools  
[GitLab Presentation
](https://drive.google.com/file/d/1uvABY49mRhYCSXbJIwpyl5zRFsuaAHH0/view)  
[Git Preso by Ilya
](https://drive.google.com/file/d/1wIEjZrTbjcucGVISimNNYmVn_ilanU3Q/view)  
[Blockchain 101 rec by Gabor
](https://www.youtube.com/watch?v=_160oMzblY8)  
[Git Links
](https://docs.google.com/document/d/1X06c7-aImMVjGH7pP2GszXHptSAHOBoad32Nabjfolw/edit)  
[Library Genesis
](http://gen.lib.rus.ec/)  
[Barcelona table and furniture days
](https://www.google.com/maps/d/u/0/viewer?mid=1l2VAhplHwkWYhNi6WOcDeqnxPoE&shorturl=1&ll=41.392682746525104%2C2.1641775000000507&z=12)  
